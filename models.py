import sys
sys.path.append('lib')

import random
from sqlalchemy import Column, Integer, String
from database import Base, engine


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    email = Column(String(200), unique=True)
    password = Column(String(100))
    received_emails = Column(String())
    library = Column(String())
    spare_dict = Column(String())

    def __init__(self, name=None, email=None, password=None, received_emails='[]', library='[]', spare_dict='{}'):
        self.id = random.randint(1, 100)
        self.name = name
        self.email = email
        self.password = password
        self.received_emails = '%s' % (received_emails)
        self.library = '%s' % (library)
        self.spare_dict = '%s' % (spare_dict)


class Track(Base):
    __tablename__ = 'track'

    id = Column(Integer, primary_key=True)
    title = Column(String(100000))
    track_id = Column(String(), unique=True)
    artist = Column(String(200))
    lyrics = Column(String(100))
    chords = Column(String())
    spare_dict = Column(String())

    def __init__(self, track_id=None, title=None, artist=None, lyrics=None, chords=None):
        self.title = artist
        self.track_id = track_id
        self.artist = title
        self.lyrics = lyrics
        self.chords = chords
        self.spare_dict = '{}'

print User.__table__
print Track.__table__