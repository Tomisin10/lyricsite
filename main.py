import sys
sys.path.append('lib')

from flask import Flask
from flask import session
from flask import request, url_for, render_template, redirect
from flask_mail import Message, Mail
import pdfkit
from database import *
from models import User, Track
from forms import SignupForm, LoginForm
import requests
import json
import redis

app = Flask(__name__)

app.secret_key = 'development key'

app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'decave12357@gmail.com'
app.config["MAIL_PASSWORD"] = 'password_of_my_email'

mail = Mail()
mail.init_app(app)

host, port = '', ''
r = redis.Redis(host=host, port=port, password='password')

class Cache(object):
    def save_song(self, track_id, artist=None, title=None, lyrics=None, chords=None):
        r.set(track_id, )
        return

    def edit_song(self, track_id, new_lyrics=None, new_chords=None):
        return

Cache = Cache()


class DatabasePipe(object):
    def get_song(self, track_id):
        song = Track.query.filter(Track.track_id == track_id).first()
        return song

    def get_user_library(self, email):
        user_file = User.query.filter(User.email == email).first()
        return user_file

    def write_user_library(self, email, new_update):
        user_file = db_session.query(User).filter(User.email == email).first()#.update({User.foo: User.foo + 1})
        user_file.update(new_update)
        db_session.commit()
        # user_file = User.query.filter(User.email == email).first()
        # user_file.update(new_update)

    def create_user_library(self, name, email, password):
        user = User(name, email, password)
        db_session.add(user)
        db_session.commit()

DB = DatabasePipe()


class LyricsAPI(object):
    url = 'http://api.musixmatch.com/ws/1.1/'
    api_key = '023d6dec43f65d97c40df5b8b3a75547'

    def get_track_id(self, title, artist):
        params = {'title':title, 'artist':artist}
        params = json.dumps(params)

        self.url += 'track.search?q_artist=%s&q_track=%s&apikey=%s' % \
              (artist, title, self.api_key)

        the_page = requests.get(self.url, params=params)
        the_page = the_page.json()

        tracks_ = the_page['message']['body']['track_list']
        search_refined = []

        #artist_name, album_coverart_350x350, track_name, track_id
        for track in tracks_:
            Cache.save_song(track['track']['track_id'], track['track']['artist_name'], track['track']['track_name'], None,
                         None)
            search_refined.append({'artist':track['track']['artist_name'], 'id':track['track']['track_id'],
                                   'title':track['track']['track_name'], 'art':track['track']['album_coverart_350x350']}
                                  )
        return search_refined

    def get_track_lyrics(self, track_id):
        self.url += 'track.lyrics.get?track_id=%s&apikey=%s' % (track_id, self.api_key)
        the_page = requests.get(self.url).json()

        lyrics = the_page['message']['body']['lyrics']['lyrics_body']
        # DB.edit_song(track_id, lyrics, None)

        return lyrics

# print(LyricsAPI().get_track_id('justin+bieber', 'Baby'))


class Utils(object):
    def send_mail(self, subject, fullname, email, message):
        msg = Message(subject, sender="contact@gmail.com")
        msg.body = """
              From: %s <%s>
              %s
              """ % (fullname, email, message)
        mail.send(msg)

    def convert_to_pdf(self):
        username = ''
        track_id = ''
        dict_of_values = {'username':username, 'track_id':track_id}
        pdfkit.from_url('/pdf_lyrics?username={username}&track_id={track_id}'.format(**dict_of_values), '')


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
    pass


@app.route('/')
@app.route('/home')
def home():
    homepage = render_template('index.html')
    return homepage, 200


@app.route('/search', methods=['GET', 'POST'])
def search():
    if request.method == 'POST':
        song_title_artist = request.form['song_title_artist']

        if not song_title_artist:
            redirect('/')

        try:
            song_artist = song_title_artist.split('-')[0]
            song_title =  song_title_artist.split('-')[1]
        except:
            song_artist, song_title = song_title_artist.split('-')[0], ''

        if song_artist.endswith(' '):
            song_artist = song_artist[:-1]
        if song_title.startswith(' '):
            song_title = song_title[1:]

        the_search_results = LyricsAPI().get_track_id( song_artist, song_title )

        return render_template('/search_results.html', search_results=the_search_results), 200

    else:
        return redirect('/')


@app.route('/lyrics', methods=['GET'])
def lyrics():
    r_id = request.args.get('id')
    the_lyrics = LyricsAPI().get_track_lyrics(r_id)
    lyrics_page = render_template('lyrics_page.html', lyrics=the_lyrics)
    return lyrics_page, 200


@app.route('/pdf_lyrics', methods=['GET'])
def pdf_lyrics():
    track_id = request.args.get('track_id')
    dict_of_values = {'track_id': track_id}
    pdfkit.from_url('/generate_pdf_html?track_id={track_id}'.format(**dict_of_values), '/tmp/%s' % track_id)


@app.route('/generate_pdf_html', methods=['GET'])
def generate_pdf_html():
    track_id = request.args.get('track_id')
    the_lyrics = LyricsAPI().get_track_lyrics(track_id)
    pdf_page = render_template('pdf_page.html', all_lyrics=[the_lyrics])

    return pdf_page, 200


@app.route('/about', methods=['GET'])
def about():
    r_id = request.args.get('id')
    the_lyrics = LyricsAPI().get_track_lyrics(r_id)
    lyrics_page = render_template('about_page.html', lyrics=the_lyrics)
    return lyrics_page, 200


@app.route('/api/pdfkit', methods=['GET'])
def api_pdfkit():
    url = request.args.get('url')
    # the_lyrics = LyricsAPI().get_track_lyrics(r_id)
    lyrics_page = pdfkit.from_url(url)
    return lyrics_page, 200


@app.route('/add_to_lib', methods=['GET'])
def add_to_lib():
    r_id = request.args.get('id')

    if 'email' in session:
        email = session['email']
        user_file = DB.get_user_library(email)

        old_lib = eval(user_file.library)
        old_lib.append(r_id)
        DB.write_user_library(email, {user_file.library: old_lib})
        return redirect(url_for('library'))

    else:
        session['next_uri'] = request.url
        return redirect(url_for('signup'))


@app.route('/library', methods=['GET', 'POST'])
def library():
    if 'email' in session:
        email = session['email']

        user_file = DB.get_user_library(email)
        name = user_file.name
        library_ = user_file.library

        print(library_)

        library_page = render_template('library_page.html', username=name, email=email, library=library_)
        return library_page, 200

    else:
        return redirect(url_for('signup'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        form = SignupForm()
        signup_page = render_template('signup_page.html', name=form.name, email=form.email, password=form.password,
                                      submit=form.submit)
        return signup_page

    elif request.method == 'POST':
        name = request.form.get('name')
        email = request.form.get('email')
        password = request.form.get('password')

        DB.create_user_library(name, email, password)
        session['email'] = email
        return redirect(url_for('library'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        form = LoginForm()
        signup_page = render_template('signup_page.html', name=form.name, password=form.password, submit=form.submit)
        return signup_page

    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        library = DB.get_user_library(email)

        if library and library.password == password:
            session['email'] = email
            return redirect(url_for('library'))

        else:
            return redirect(url_for('signup'))

# app.run(debug=True)
