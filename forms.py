#This is just a reference point for creating forms in the future
from flask_wtf import Form
from wtforms import Form, TextField, TextAreaField, SubmitField, validators, ValidationError

class SignupForm(Form):
    name = TextField("Name", [validators.DataRequired("Please enter Your Name")])
    email = TextField("Email", [validators.DataRequired("Please enter your email address"),
                                validators.email("Please enter your email address")])
    password = TextField("Password", [validators.DataRequired("Password must be more than 6 characters")])
    # subject = TextField("Subject", [validators.DataRequired("Please enter a subject.")])
    # message = TextAreaField("Message", [validators.DataRequired("Please enter a message.")])
    submit = SubmitField("SUBMIT")

class LoginForm(Form):
    name = TextField("Name", [validators.DataRequired("Please enter Your Name")], id='name-form1-q')
    email = TextField("Email", [validators.DataRequired("Please enter your email address"),
                                validators.email("Please enter your email address")], id='email-form1-q')
    password = TextField("Password", [validators.DataRequired("Password must be more than 6 characters")],
                         id='password-form1-q')
    # subject = TextField("Subject", [validators.DataRequired("Please enter a subject.")])
    # message = TextAreaField("Message", [validators.DataRequired("Please enter a message.")])
    submit = SubmitField("SUBMIT")