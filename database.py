import sys
sys.path.append('lib')

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('sqlite:///tmp/box.db', convert_unicode=True, echo=True)
metadata = MetaData(bind=engine)
users = Table('users', metadata,
              Column('id', Integer, primary_key=True),
              Column('name', String), Column('email', String), Column('password', String),
              Column('received_emails', String), Column('library', String), Column('spare_dict', String))

tracks = Table('tracks', metadata,
               Column('id', Integer, primary_key=True),
               Column('title', String), Column('track_id', String), Column('artist', String),
               Column('lyrics', String), Column('chords', String), Column('spare_dict', String))
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

metadata.create_all(bind=engine)
for _t in metadata.tables:
    print('Table: %s' % _t)
